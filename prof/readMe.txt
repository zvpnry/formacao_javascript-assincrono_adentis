CURSO Javascript Assíncrono e API's de HTML5 - AGAP


O Javascript é uma linguagem “multi-paradigma”, podendo ser utilizada em ambientes de desenvolvimento Object Oriented (prototype based), Imperative ou Functional Programming.
Javascript, é uma linguagem “single-thread”, ou seja o “interpretador” de Javascript, só consegue lidar com um processo de cada vez,  em operações de comunicação com API’s externas, base de dados,  operações de Input/output, entre outras, podem levar a que a aplicação fique bloqueada. É aqui que entram os conceitos de Javascript assíncrono, que através de API’s e técnicas como “callbacks”, “Promises”, “async / await funcrtions”, permite-nos gerir vários processos sem “bloquear”a “Main Thread”  do Javascript.

Pretende-se com este curso, transmitir e atualizar conhecimentos, skills,  utilizando boas práticas na utilização de Javascript assíncrono e utilização destas técnicas em conjunto com algumas API’s de HTML5, como Canvas, Geolocation, Webstorage, FileApi, Fetch, WebSockets, entre outras.

Conteúdo Programático:
* Revisão de conceitos fundamentais em Javascript, especificações ES6+
(Scope, IIFE’s, Closure’s, This & Context, call / apply / bind, arrow functions, default parameters, spread and Rest operators, destructuring de Arrays e Objects, function generators, novas formas de iteração, em array e objectos, Classes e Modules, ...)
* Conceitos Genéricos de programação Assíncrona.
* Introdução a conceitos e técnicas assíncronas em Javascript
* Gerir operações assíncronas em Javascript com Promises
* Gerir operações assíncronas com funções “async/await"
* Utilização da Fetch API, para chamadas assíncronas a API’s externas.
* Utilização de algumas das mais importantes API’s de HTML5
(FileApi, Geolocation, Canvas, WebStorage, entre outras)

Duração: 15 Horas

Datas : 3, 8, 10, 15 e 17 de Setembro

Plataforma : GotoMeeting, acesso á sala “online:
AGAP-JS-ASYNC
Please join my meeting from your computer, tablet or smartphone.
https://global.gotomeeting.com/join/123358053
Access Code: 123-358-053

Pré-Requisitos:
Conhecimentos sólidos em Javascript, de preferência com conhecimentos das novas especificações em ES6+.
 
Destinatários:
Developers  Web ou de outros ambientes de desenvolvimento, ou profissionais interessados aprofundarem  e/ou adquirir novas competências no desenvolvimento de aplicações web de acordo com as mais recentes tendencias, boas práticas e standards atuais em Javascript.


--------------------------------------------------------------------------

Ambiente de desenvolvimento:

Pessoalmente apesar de usar mais o "webstorm" para desenvolvimento web de front-end, irei utilizar como de costume o VSCode (https://code.visualstudio.com/) durante a formação, peço-lhe que instalem com as seguintes extensoes(deixei umas imagens  se tiverem duvidas na pasta):
*Javascript (ES6) snippets
*live server
*node.js exec

--------------------------------------------------------------------------